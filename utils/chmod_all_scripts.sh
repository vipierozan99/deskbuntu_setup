#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

for file in ../bundles/**/*.sh; do
    chmod +x "$file" -v
done
