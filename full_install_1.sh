#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

bash ./bundles/default_tools/install.sh

bash ./bundles/micro/install.sh

bash ./bundles/gnome/install.sh

cat << EOF
## INSTALL PART 1 DONE ##
      PLEASE REBOOT
EOF
