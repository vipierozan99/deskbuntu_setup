#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo apt install openocd
sudo ln -sf  "$(realpath ./99-openocd.rules)" /etc/udev/rules.d/99-openocd.rules

cat << EOF
Please restart for udev rules to take effect
EOF