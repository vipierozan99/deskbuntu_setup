#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo grub-editenv - set boot_once_timeout=0
sudo grub-reboot 0
sudo reboot
