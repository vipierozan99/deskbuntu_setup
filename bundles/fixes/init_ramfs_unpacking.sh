#! /bin/bash

# “Initramfs unpacking failed: Decoding failed”. How to fix this error at the boot-up time.
# https://masterinformers.com/initramfs-unpacking-failed-decoding-failed/

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

file=/etc/initramfs-tools/initramfs.conf 

# Change compression to gzip
sudo sed 's/COMPRESS=.*/COMPRESS=gzip/' "$file" | sudo tee "$file"

# Apply
sudo update-initramfs -u

# Should work on next reboot


