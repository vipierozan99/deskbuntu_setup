#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
# sudo chmod a+rx /usr/local/bin/youtube-dl

git clone https://github.com/ytdl-org/youtube-dl.git || :

cd youtube-dl
sudo make install