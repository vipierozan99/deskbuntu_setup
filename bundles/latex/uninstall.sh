#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo rm -rf /usr/local/texlive/2020
sudo rm -rf ~/.texlive2020

sudo rm -rf ./install_dir