#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sed 's/modifier_map Mod3   { Scroll_Lock };/#modifier_map Mod3   { Scroll_Lock };/' /usr/share/X11/xkb/symbols/br | sudo tee /usr/share/X11/xkb/symbols/br