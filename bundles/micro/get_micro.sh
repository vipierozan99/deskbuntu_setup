#!/bin/sh

set -e -u


while getopts ":o:" opt; do
  case $opt in
    o) out="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

printf "Argument out is %s\n" "$out"

githubLatestTag() {
  finalUrl=$(curl "https://github.com/$1/releases/latest" -s -L -I -o /dev/null -w '%{url_effective}')
  printf "%s\n" "${finalUrl##*v}"
}

platform='amd64'
TAG=$(githubLatestTag zyedidia/micro)
extension='deb'

printf "Latest Version: %s\n" "$TAG"
printf "Downloading https://github.com/zyedidia/micro/releases/download/v%s/micro-%s-%s.%s\n" "$TAG" "$TAG" "$platform" "$extension"

curl -L "https://github.com/zyedidia/micro/releases/download/v$TAG/micro-$TAG-$platform.$extension" > "$out/micro.$extension"

