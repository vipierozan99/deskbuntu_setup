#! /bin/bash

set -e -u

cd "$(dirname "${BASH_SOURCE[0]}")"

mkdir install_dir
cd install_dir
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

tar -xzf install-tl-unx.tar.gz
rm install-tl-unx.tar.gz

cd install-tl*
sudo ./install-tl --profile=../../texlive.profile

# update paths
tee -a ~/.bashrc << 'EOF'
# latex texlive
PATH=/usr/local/texlive/2020/bin/x86_64-linux:$PATH; export PATH
MANPATH=/usr/local/texlive/2020/texmf-dist/doc/man:$MANPATH; export MANPATH
INFOPATH=/usr/local/texlive/2020/texmf-dist/doc/info:$INFOPATH; export INFOPATH

EOF


# cleanup
cd ../..
sudo rm -rf install_dir