#! /bin/bash

set -e -u

lsusb -v 2> /dev/null | grep -e "Apple Inc" -A 2 | awk 'FNR == 3 {print $3}'

# Using the UUID, press Ctrl + L in nautilus and type afc://<UUID>