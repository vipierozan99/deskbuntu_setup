#! /bin/bash

# https://www.cyberciti.biz/faq/how-to-install-networked-hp-printer-and-scanner-on-ubuntu-linux/

set -e -u

cd "$(dirname "${BASH_SOURCE[0]}")"

sudo apt install hplip hplib-gui simple-scan