#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

mkdir -p "$HOME"/packages

# Get latest .deb
./get_micro.sh -o "$HOME"/packages

# Clipboard support
sudo apt install -y xclip

sudo apt install -y "$HOME"/packages/micro.deb

# Apply configs
mkdir -p "$HOME"/.config/micro
ln -sf "$(realpath ./settings.json)" "$HOME"/.config/micro/settings.json

xdg-icon-resource install "$(realpath ./micro-logo.xpm)" --size 64 --novendor
sudo ln -sf "$(realpath ./micro.desktop)" "/usr/share/applications/micro.desktop"

