#! /bin/bash

# https://www.rust-lang.org/tools/install

set -e -u

cd "$(dirname "${BASH_SOURCE[0]}")"

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

