#! /bin/bash

# https://linuxhandbook.com/nicehash-linux/

set -e -u

cd "$(dirname "${BASH_SOURCE[0]}")"

sudo apt install python3-pip

sudo pip3 install git+https://github.com/YoRyan/nuxhash