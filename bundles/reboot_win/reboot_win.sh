#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo grub-editenv - set boot_once_timeout=0

windows_entry=$(awk -F\' '/menuentry / {print $2}' /boot/grub/grub.cfg | grep Windows)

sudo grub-reboot "$windows_entry"
sudo reboot
