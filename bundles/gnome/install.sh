#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# Install shell and deps
sudo apt install -y \
    gnome-shell \
    gnome-terminal \
    gnome-tweak-tool \
    file-roller \
    fonts-noto-color-emoji


# Enable service at startup
# sudo systemctl enable gdm
