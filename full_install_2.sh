#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

bash ./bundles/gnome_ext/install.sh

bash ./bundles/dunst/install.sh

bash ./bundles/brave/install.sh

bash ./bundles/vscode/install.sh

cat << EOF
## INSTALL PART 2 DONE ##
         ENJOY!
EOF