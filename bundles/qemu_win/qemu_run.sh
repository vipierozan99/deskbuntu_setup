#! /bin/bash

# https://superuser.com/questions/342719/how-to-boot-a-physical-windows-partition-with-qemu
# https://www.reddit.com/r/VFIO/comments/aqk2pk/qemukvm_4k_resolutions_using_qxl_via_command_line/
# https://wiki.gentoo.org/wiki/QEMU/Windows_guest
# https://stafwag.github.io/blog/blog/2018/04/22/high-screen-resolution-on-a-kvm-virtual-machine-with-qxl/

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

qemu-system-x86_64 \
    -enable-kvm                                   `# enable KVM optimiations` \
    -L .                                          `# dir with bios.bin` \
    --bios bios.bin                               `# bios.bin itself` \
    -m 8G                                         `# provide reasonable amount of ram` \
    -cpu host                                     `# match the CPU type exactly` \
    -drive file=/dev/sdb,format=raw,media=disk    `# load raw HDD`  \
    -enable-kvm  `# enable hyper-v enlightenments` \
    -cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time `# emulate exact host cpu` \
    -machine type=pc,accel=kvm \
    -smp "$(($(nproc) - 2))" `# use all - 2 available CPU cores` \
    -vga qxl \
    -global qxl-vga.vgamem_mb=32
    # -daemonize  `# don't start monitor, we connect using RDP` \
    # -spice port=5930,disable-ticketing \
    # -device virtio-serial \
    # -device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
    # -chardev spicevmc,id=spicechannel0,name=vdagent 