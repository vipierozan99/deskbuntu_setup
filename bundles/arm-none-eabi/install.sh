#! /bin/bash

# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
# Check latest release here

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo apt-get install libncurses5

install_dir=$HOME/Downloads
filename="gcc-arm-none-eabi.tar.bz2"
url="https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2?revision=ca0cbf9c-9de2-491c-ac48-898b5bbc0443&la=en&hash=68760A8AE66026BCF99F05AC017A6A50C6FD832A"

wget -q --show-progress -O "$filename" "$url"
mv "$filename" "$install_dir"
tar -xf "$install_dir"/"$filename"
rm "$install_dir"/"$filename"

installed_path="$install_dir/gcc-arm-none-eabi-*"

for bin in $(realpath $installed_path/bin/*)
do
    sudo ln -sfv "$bin" /usr/local/bin
done
