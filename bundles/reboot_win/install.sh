#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo cp ./50_rebootwin /etc/grub.d

sudo update-grub
sudo ln -sf  "$(realpath ./reboot_win.sh)" /usr/bin/reboot.windows
sudo ln -sf  "$(realpath ./reboot_linux.sh)" /usr/bin/reboot.linux


