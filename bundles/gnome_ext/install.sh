#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# Install theme packages
sudo apt install -y \
    yaru-theme-gnome-shell \
    yaru-theme-gtk \
    yaru-theme-icon \
    yaru-theme-sound \

# Install gnome-shell-extension-installer
wget -O gnome-shell-extension-installer "https://github.com/brunelli/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
chmod +x gnome-shell-extension-installer
sudo mv gnome-shell-extension-installer /usr/bin/

# Link profile file
sudo ln -sf "$(realpath ./user)" /etc/dconf/profile/user

# Link defaults profile
sudo mkdir -p /etc/dconf/db/local.d
sudo ln -sf "$(realpath ./01-defaults)" /etc/dconf/db/local.d/01-defaults

# Update dconf db
sudo dconf update

# 307: dash-to-dock
# 906: Sound device chooser
# 744: Hide Activities Button
gnome-shell-extension-installer 307 906 744 --yes --restart-shell

# # Restart Gnome session
# killall -SIGQUIT gnome-shell || true
