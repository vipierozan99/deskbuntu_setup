#! /bin/bash

# Try to rename .mp3 files to the title in their metadata

set -e -u

output_dir="$HOME/Music/iphone"

get_song_name() {
    id3tool "$1" |  awk 'FNR == 2' | cut -d ":" -f2 | sed -e 's/^[ \t]*//' | sed -e 's/[ \t]*$//'
}

cp_and_rename() {
    file=$(realpath "$1")
    song_name=$(get_song_name "$file")
    if [ "$song_name" != "No ID3 Tag" ]
    then
        cp "$file" "$output_dir/$song_name.mp3"
    else
        cp "$file" "$output_dir/$(basename "$file")"
    fi   
}


for f in ~/Music/iphone/**/*.mp3
do
    echo "$f"
    cp_and_rename "$f"
done