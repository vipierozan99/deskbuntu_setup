#! /bin/bash

set -e -u
# cd "$(dirname "${BASH_SOURCE[0]}")"

if [ -z "$(xdotool search --desktop 0 --limit 1 --class terminal)" ]; then
    echo "No terminal open, launching one.";
    gnome-terminal &
fi

WINDOWS=$(xdotool search --desktop 0 --limit 1 --class terminal)

for WINDOW in $WINDOWS; do
    # xdotool getwindowname "${WINDOW}"
    xdotool windowactivate "${WINDOW}"
    xdotool windowfocus "${WINDOW}"
done