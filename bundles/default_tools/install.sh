#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"


# Install basic tools
sudo apt install -y \
    git \
    curl \
    make \
    software-properties-common \
    apt-transport-https \
    wget

# Setup git
git config --global user.name "Victor E. Pierozan"
git config --global user.email "victorpierozan@gmail.com"

