from sympy import init_session, interactive
from matplotlib import style
from collections import UserDict

style.use("seaborn-whitegrid")
interactive.session.preexec_source = """\
from sympy import Symbol, Integer, Eq, solve
import numpy as np
import math
import matplotlib.pyplot as plt
"""
init_session(quiet=False, auto_symbols=True, auto_int_to_Integer=True)


class Consts(UserDict):
    def subs(self, _eqs):
        try:
            iterator = iter(_eqs)
            return list(map(lambda x: x.subs(self.data), iterator))
        except TypeError:
            return _eqs.subs(self.data)

    def solve(self, _eqs, *args, **kwargs):
        from sympy import solve

        return solve(self.subs(_eqs), *args, dict=True, **kwargs)


consts = Consts()
