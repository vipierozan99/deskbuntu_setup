#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

sudo apt install -y dunst

# Create symlinks to config files
mkdir -p "$HOME"/.config/dunst
for f in files/*; do
    ln -sf "$(realpath $f)" "$HOME"/.config/dunst/$(basename $f)
done;


# Disable Gnome notification service
sudo mv /usr/share/dbus-1/services/org.gnome.Shell.Notifications.service /usr/share/dbus-1/services/org.gnome.Shell.Notifications.service.disabled || true

# Kill Gnome process
kill "$(pidof /usr/bin/gjs)" || true

# Enable service
systemctl enable --user dunst.service
systemctl start --user dunst.service
