#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# find first file in /etc/netplan  
# The glob expansion uses alphabetical order 
for file in /etc/netplan/*.yaml; do
    netcfg=$(realpath "$file")
    break 1
done

# Substitute the renderer the write to file
sed 's/renderer: .*/renderer: NetworkManager/' "$netcfg" | sudo tee "$netcfg"

# Reload config
sudo netplan apply




