#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# Get key
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -

# Get repo
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

sudo apt update
sudo apt install -y code
