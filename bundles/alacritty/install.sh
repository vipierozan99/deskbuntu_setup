#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# add config
mkdir -p "$HOME/.config/alacritty"
sudo ln -s "$(realpath ./alacritty.yml)" "$HOME/.config/alacritty/alacritty.yml"

pushd "$HOME/code"

# clone repo
git clone https://github.com/alacritty/alacritty.git || true
cd alacritty

# install deps
sudo apt install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3

# build
cargo build --release

# add to PATH
sudo ln -s "$(realpath ./target/release/alacritty)" /usr/local/bin/alacritty


# install manpages
sudo mkdir -p /usr/local/share/man/man1
gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null