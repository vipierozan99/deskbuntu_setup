#! /bin/bash

set -e -u
cd "$(dirname "${BASH_SOURCE[0]}")"

# bash aliases
ln -s "$(realpath .bash_aliases)" $HOME/.bash_aliases

# # bringup_term: bring up the terminal
# sudo apt install xdotool
# sudo ln -s "$(realpath bringup_term.sh)" /usr/local/bin/bringup_term  || :


